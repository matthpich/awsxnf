timeline {
  enabled = true
  file = "execution_timeline.html"
}

report {
  enabled = true
  file = "execution_report.html"
}

trace {
  enabled = true
  file = "execution_trace.txt"
}

dag {
  enabled = true
  file = "pipeline_dag.svg"
}

profiles {

    conda {
        conda    = "python=3.6 docopt"
    }
    
    docker {
        container = "matthpich/metagenomic:latest"
        docker.enabled = true
    }

    local {
        process {
            executor = "local"
            cleanup  = true
        }
    }

    hpc {
        process {
            executor = "sge"
            //errorStrategy = "ignore"
            cleanup  = true
            withLabel: basic {
                clusterOptions = '-S /bin/bash -l m_mem_free=8G -pe smp 1 -binding linear:1'
            }         
            withLabel: multi_cpu {
                clusterOptions = '-S /bin/bash -l m_mem_free=12.5G -pe smp 4 -binding linear:4'
            }
            withLabel: multi_cpu_large_mem {
                clusterOptions = '-S /bin/bash -l m_mem_free=20G -pe smp 4 -binding linear:4'
            }
            withLabel: many_cpu {
                clusterOptions = '-S /bin/bash -l m_mem_free=6.25G -pe smp 8 -binding linear:8'
            }
        }
   }

    aws {
        aws {
            client.storageEncryption = 'AES256'
            batch.cliPath = '/home/ec2-user/miniconda/bin/aws'
        }
        process {
            executor = "awsbatch"
            queue    = "NFqueue_optimal"
            //errorStrategy = 'ignore'
            cleanup  = true
            withLabel: simple {
                memory = 1.GB
                cpus   = 1 
            }
            withLabel: basic {
                memory = 8.GB
                cpus   = 1
            }
            withLabel: multi_cpu {
                memory = 32.GB
                cpus   = 8
            }
            withLabel: multi_cpu_large_mem {
                memory = 80.GB
                cpus   = 8
            }    
        }
    }
}
